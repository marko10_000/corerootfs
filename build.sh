#! /bin/sh
cd "$(dirname "$0")" &&
for i in i686 riscv64 x86_64
do
	bst -o target_arch "$i" build all.bst &&
	mkdir -p output/"$i" &&
	bst -o target_arch "$i" checkout core.bst --tar - | lzip --best > output/"$i"/core.tar.lz &&
	bst -o target_arch "$i" checkout default-links.bst --tar - | lzip --best > output/"$i"/default-links.tar.lz &&
	bst -o target_arch "$i" checkout dev.bst --tar - | lzip --best > output/"$i"/dev.tar.lz || exit 1
done
