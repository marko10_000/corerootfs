#! /usr/bin/env bash

#
# Config
#
if [ "$1" == 'x86_64' ]
then
	true
else
	echo "Unknown architecture $1" >&2
	exit 1
fi
TMP_SYSROOT="$(realpath "./tmp")" &&
ARCH="$1-linux" &&
OUTPUT_DIR="$(realpath "./output")" &&

#
# Download
#
(
	if [ ! -f 'gcc-11.2.0.tar.xz' ]
	then
		wget https://ftp.gnu.org/gnu/gcc/gcc-11.2.0/gcc-11.2.0.tar.xz
	fi &&
	if [ ! -f 'gmp-6.2.1.tar.lz' ]
	then
		wget https://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.lz
	fi &&
	if [ ! -f 'mpc-1.2.1.tar.gz' ]
	then
		wget https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz
	fi &&
	if [ ! -f 'mpfr-4.1.0.tar.xz' ]
	then
		wget https://ftp.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.xz
	fi &&
	if [ ! -f 'binutils-2.37.tar.lz' ]
	then
		wget https://ftp.gnu.org/gnu/binutils/binutils-2.37.tar.lz
	fi &&
	if [ ! -f 'musl-1.2.2.tar.gz' ]
	then
		wget https://musl.libc.org/releases/musl-1.2.2.tar.gz
	fi &&
	if [ ! -f 'coreutils-8.32.tar.xz' ]
	then
		wget https://ftp.gnu.org/gnu/coreutils/coreutils-8.32.tar.xz
	fi &&
	if [ ! -f 'dash-0.5.11.4.tar.gz' ]
	then
		wget https://git.kernel.org/pub/scm/utils/dash/dash.git/snapshot/dash-0.5.11.4.tar.gz
	fi
) &&

#
# Binutils stage1
#
( if [ -z "$NO_BINUTILS_STAGE_1" ]
then
	rm -rf "$TMP_SYSROOT" &&
	rm -rf binutils &&
	tar --lzip -xf binutils-2.37.tar.lz &&
	mv binutils-2.37 binutils &&
	mkdir -p binutils/stage1 &&
	cd binutils/stage1 &&
	../configure \
	--disable-nls \
	--disable-werror \
	--target="$ARCH" \
	--prefix="$TMP_SYSROOT/usr" \
	--with-sysroot="$TMP_SYSROOT" &&
	make "-j$(nproc)" all-binutils all-gas all-ld &&
	make install-binutils install-gas install-ld &&
	ln -s $ARCH-ar $TMP_SYSROOT/usr/bin/$ARCH-musl-ar &&
	ln -s $ARCH-as $TMP_SYSROOT/usr/bin/$ARCH-musl-as &&
	ln -s $ARCH-ranlib $TMP_SYSROOT/usr/bin/$ARCH-musl-ranlib &&
	ln -s $ARCH-ld $TMP_SYSROOT/usr/bin/$ARCH-musl-ld &&
	ln -s $ARCH $TMP_SYSROOT/usr/$ARCH-musl
fi ) &&

#
# Generate gcc stage1
#
PATH="$TMP_SYSROOT/usr/bin:$PATH" &&
( if [ -z "$NO_GCC_STAGE_1" ]
then
	rm -rf gcc &&
	tar --xz -xaf gcc-11.2.0.tar.xz &&
	tar --lzip -xaf gmp-6.2.1.tar.lz &&
	tar -xzaf mpc-1.2.1.tar.gz &&
	tar --xz -xaf mpfr-4.1.0.tar.xz &&
	mv gcc-11.2.0 gcc &&
	mv gmp-6.2.1 gcc/gmp &&
	mv mpc-1.2.1 gcc/mpc &&
	mv mpfr-4.1.0 gcc/mpfr &&
	mkdir -p gcc/build1 &&
	cd gcc/build1 &&
	sed -i 's|\./fixinc\.sh|-c true|' ../gcc/Makefile.in &&
	../configure \
	--disable-bootstrap \
	--disable-decimal-float \
	--disable-libatomic \
	--disable-libgomp \
	--disable-libquadmath \
	--disable-libssp \
	--disable-libstdcxx \
	--disable-libvtv \
	--disable-lto \
	--disable-nls \
	--disable-multilib \
	--disable-shared \
	--disable-threads \
	--enable-initfini-array \
	--enable-languages=c,c++ \
	--enable-static \
	--prefix="$TMP_SYSROOT/usr" \
	--target="$ARCH-musl" \
	--with-newlib \
	--with-sysroot="$TMP_SYSROOT" \
	--without-headers &&
	make "-j$(nproc)" &&
	make install
fi ) &&

#
# Musl
#
( if [ -z "$NO_MUSL" ]
then
	rm -rf musl &&
	rm -rf "$OUTPUT_DIR" &&
	tar -xzf musl-1.2.2.tar.gz &&
	mv musl-1.2.2 musl &&
	mkdir musl/build &&
	cd musl/build &&
	../configure \
	--disable-shared \
	--enable-static \
	--prefix=/usr \
	--target="$ARCH-musl" \
	AR="$ARCH-ar" \
	LD="$ARCH-ld" \
	RANLIB="$ARCH-ranlib" \
	CC="$ARCH-musl-gcc" \
	CFLAGS="-fPIC -Os" &&
	make "-j$(nproc)" &&
	make install DESTDIR="$OUTPUT_DIR" &&
	make install DESTDIR="$TMP_SYSROOT"
fi ) &&

#
# Libstdc++
#
( if [ -z "$NO_GCC_STAGE_2" ]
then
	rm -rf gcc/build2 &&
	mkdir -p gcc/build2 &&
	cd gcc/build2 &&
	../libstdc++-v3/configure \
	--build="$ARCH-musl" \
	--disable-multilib \
	--disable-nls \
	--disable-libstdcxx-pch \
	--host="$ARCH-musl" \
	--target="$ARCH-musl" \
	--prefix=/usr \
	AR="$ARCH-musl-ar" \
	LD="$ARCH-musl-ld" \
	RANLIB="$ARCH-musl-ranlib" \
	CC="$ARCH-musl-gcc" \
	CXX="$ARCH-musl-g++" \
	CFLAGS="-fPIC -Os" \
	CXXFLAGS='-Os' &&
	make "-j$(nproc)" &&
	make "-j$(nproc)" install DESTDIR="$TMP_SYSROOT"
fi ) &&

#
# Binutils stage2
#
( if [ -z "$NO_BINUTILS_STAGE_2" ]
then
	rm -rf binutils/stage2 &&
	mkdir -p binutils/stage2 &&
	cd binutils/stage2 &&
	../configure \
	--build="$ARCH-musl" \
	--disable-nls \
	--disable-shared \
	--disable-werror \
	--enable-deterministic-archives \
	--enable-ld=default \
	--enable-lto \
	--enable-multiarch \
	--enable-multilib \
	--enable-static \
	--with-glibc-version=2.11 \
	--with-newlib \
	--host="$ARCH-musl" \
	--prefix=/usr \
	--without-debuginfod \
	AR="$ARCH-musl-ar" \
	LD="$ARCH-musl-ld" \
	LDFLAGS="-static" \
	RANLIB="$ARCH-musl-ranlib" \
	CC="$ARCH-musl-gcc" \
	CXX="$ARCH-musl-g++" \
	CFLAGS='-Os' \
	CXXFLAGS='-Os' &&
	make "-j$(nproc)" all-binutils all-gas all-ld &&
	make install-binutils install-gas install-ld DESTDIR="$OUTPUT_DIR"
fi ) &&

#
# Coreutils
#
( if [ -z "$NO_COREUTILS" ]
then
	rm -rf coreutils &&
	tar --xz -xf coreutils-8.32.tar.xz &&
	mv coreutils-8.32 coreutils &&
	mkdir -p coreutils/build &&
	cd coreutils/build &&
	../configure \
	--disable-nls \
	--disable-shared \
	--enable-single-binary=shebangs \
	--enable-static \
	--prefix=/usr \
	AR="$ARCH-musl-ar" \
	LD="$ARCH-musl-ld" \
	LDFLAGS="-static" \
	RANLIB="$ARCH-musl-ranlib" \
	CC="$ARCH-musl-gcc" \
	CXX="$ARCH-musl-g++" \
	CFLAGS='-Os' \
	CXXFLAGS='-Os' &&
	make "-j$(nproc)" &&
	make "-j$(nproc)" install DESTDIR="$OUTPUT_DIR"
fi ) &&

#
# Dash
#
( if [ -z "$NO_DASH" ]
then
	rm -rf dash &&
	tar -xzf dash-0.5.11.4.tar.gz &&
	mv dash-0.5.11.4 dash &&
	mkdir -p dash/build &&
	cd dash &&
	./autogen.sh &&
	cd build &&
	../configure \
	--disable-nls \
	--disable-shared \
	--enable-static \
	--prefix=/usr \
	AR="$ARCH-musl-ar" \
	LD="$ARCH-musl-ld" \
	LDFLAGS="-static" \
	RANLIB="$ARCH-musl-ranlib" \
	CC="$ARCH-musl-gcc" \
	CFLAGS='-DPATH_MAX=1024 -DNAME_MAX=1024 -Os' \
	CXX="$ARCH-musl-g++" &&
	make "-j$(nproc)" &&
	make "-j$(nproc)" install DESTDIR="$OUTPUT_DIR"
fi ) &&

#
# Gcc stage3
#
( if [ -z "$NO_GCC_STAGE_3" ]
then
	echo '#error No support' > "$TMP_SYSROOT/usr/include/zstd.h"
	rm -rf gcc/build3 &&
	mkdir -p gcc/build3 &&
	rm $TMP_SYSROOT/usr/$ARCH-musl &&
	ln -s . $TMP_SYSROOT/usr/$ARCH-musl &&
	cd gcc/build3 &&
	../configure \
	--disable-bootstrap \
	--disable-decimal-float \
	--disable-libatomic \
	--disable-libgomp \
	--disable-libquadmath \
	--disable-libssp \
	--disable-libstdcxx \
	--disable-libvtv \
	--disable-lto \
	--disable-nls \
	--disable-multilib \
	--disable-shared \
	--disable-threads \
	--enable-initfini-array \
	--enable-languages=lto,c,c++ \
	--enable-static \
	--with-newlib \
	--host="$ARCH-musl" \
	--prefix="/usr" \
	--target="$ARCH-musl" \
	AR="$ARCH-musl-ar" \
	LD="$ARCH-musl-ld" \
	LDFLAGS="-static" \
	RANLIB="$ARCH-musl-ranlib" \
	CC="$ARCH-musl-gcc" \
	CXX="$ARCH-musl-g++" \
	CFLAGS='-Os' \
	CXXFLAGS='-Os' &&
	#CFLAGS='-D_GLIBCXX_HAVE_FENV_H' \
	#CXXFLAGS='-D_GLIBCXX_HAVE_FENV_H' &&
	make "-j$(nproc)" &&
	make install DESTDIR="$OUTPUT_DIR"
fi ) &&

#
# Gcc stage4
#
( if [ -z "$NO_GCC_STAGE_4" ]
then
	rm -rf gcc/build4 &&
	mkdir -p gcc/build4 &&
	cd gcc/build4 &&
	../libstdc++-v3/configure \
	--build="$ARCH-musl" \
	--disable-multilib \
	--disable-nls \
	--disable-libstdcxx-pch \
	--host="$ARCH-musl" \
	--target="$ARCH-musl" \
	--prefix=/usr \
	AR="$ARCH-musl-ar" \
	LD="$ARCH-musl-ld" \
	RANLIB="$ARCH-musl-ranlib" \
	CC="$ARCH-musl-gcc" \
	CXX="$ARCH-musl-g++" \
	CFLAGS="-fPIC -Os" \
	CXXFLAGS='-Os' &&
	( make "-j$(nproc)" -k || true ) &&
	rm -v include/fenv.h &&
	make "-j$(nproc)" &&
	make "-j$(nproc)" install DESTDIR="$OUTPUT_DIR"
fi ) &&

#
# Strip, finisch and pack
#
( if [ -z "$NO_STRIP" ]
then
	find "$OUTPUT_DIR/usr/libexec" "$OUTPUT_DIR/usr/bin" | xargs -I{} "$ARCH-musl-strip" {}
	ln -s dash "$OUTPUT_DIR/usr/bin/sh" &&
	tar -cv --group=nobody --owner=nobody output | lzip --best > v1.0-$ARCH.tar.lz
fi )
